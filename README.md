![](https://elioway.gitlab.io/eliobones/elio-Bones-logo.png)

> Measuring the quanta of life, **the elioWay**

# eliobones ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

**eliobones** groups up <https://schema.org> rooted Restful APIs, **the elioWay**.

We call them "endpoints" and they are all identically formatted to take an **engaged** "thing" and its **list**, transform and return it.

- [eliobones Documentation](https://elioway.gitlab.io/eliobones/)

## Installing

- [Installing eliobones](https://elioway.gitlab.io/eliobones/installing.html)

## Requirements

- [elioway Prerequisites](https://elioway.gitlab.io/installing.html)

## Seeing is Believing

- [elioway Quickstart](https://elioway.gitlab.io/quickstart.html)
- [eliobones Quickstart](https://elioway.gitlab.io/eliobones/quickstart.html)

# Credits

- [eliobones Credits](https://elioway.gitlab.io/eliobones/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/eliobones/apple-touch-icon.png)
