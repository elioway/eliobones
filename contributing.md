# Contributing

```shell
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git submodule init
git submodule update
# or
set L adon eve god innocent sassy-fibonacciness generator-sin icon sins toothpaste bushido
for R in $L
    git submodule add git@gitlab.com:eliosin/$R.git
end
```
