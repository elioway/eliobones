# Contributing

```shell
git clone https://gitlab.com/elioway/eliobones.git
cd eliobones
git submodule init
git submodule update
# or
set L bones node-bones mongoose-bones feather-bones mongo-daemon
for R in $L
    git submodule add git@gitlab.com:eliobones/$R.git
end
```

## TODOS

1. The `bones` folder is being moved to **mongoose-bones**. Leave just useful, sharable non-mongoose stuff.
