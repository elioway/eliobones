> The crappy, tedious chores. "But they _do_ have to be written!", you say. Correct. Every time. So why can't we change that? **Tim Bushell**

# Ribs

<aside>
  <dl>
  <dd>Giants of mighty bone and bold emprise;</dd>
  <dd>Part wield their arms, part curb the foaming steed,</dd>
  <dd>Single or in array of battle ranged</dd>
</dl>
</aside>

Developers, `create` `read` `update` and `delete` no more. We'll do them and we'll do them well, so you don't have to.

**eliobones** provides endpoints for manipulating a "thing"'s data, according to the _design_pattern_.

**eliobones** is a collection of Restful APIs and servers, for <https://schema.org> _schemaTypes_, supporting client applications.

[eliobones Dogma](/eliobones/dogma.html)

At its heart are the _ribs_ of **bones**, a collection of endpoints you can call. These always operate on a whole "thing" of data.

But there more to it than that: These endpoints are merely the CRUDDY requirements every app has, but they are also the blue print for creating custom endpoints for your applications which can perform such tasks as `enrolStudentT`, `findBestDescriptionT`, `convertImperialToMetricT`, `canWeSaveTheDolphinsT`

<article>
  <a href="/eliobones/bones/">
  <img src="/eliobones/bones/favicoff.png">
  <div>
  <h4>bones</h4>
  <p>Reusable endpoints for <strong>eliobones</strong> projects, and a CLI for calling them, the <strong>elioWay</strong>.</p>
</div>
</a>
</article>

<article>
  <a href="/eliobones/generator-ribs/">
  <img src="/eliobones/generator-ribs/favicoff.png">
  <div>
  <h4>generator-ribs</h4>
  <p>A yeoman generator wrapping <strong>bones</strong>, for the more cautious, the <strong>elioWay</strong>.
    </p>
</div>
</a>
</article>

## Skeletons

<section>
  <figure>
  <a href="/eliobones/node-bones/artwork/splash.jpg" target="_splash"><img src="/eliobones/node-bones/artwork/splash.jpg" target="_splash"></a>
  <h3>NodeJs</h3>
  <p>Rest API in <a href="https://nodejs.org/">NodeJs</a> with zero dependencies, the <strong>elioWay</strong>.</p>
  <p><a href="/eliobones/node-bones/"><button><img src="/eliobones/node-bones/apple-touch-icon.png"><br>node-bones</button></a></p>
</figure>
  <figure>
  <a href="/eliobones/dbhell-bones/artwork/splash.jpg" target="_splash"><img src="/eliobones/dbhell-bones/artwork/splash.jpg" target="_splash"></a>
  <h3>dbhell</h3>
  <p>File system database written in NodeJs with only lodash dependencies, the <strong>elioWay</strong>.</p>
  <p><a href="/eliobones/dbhell-bones/"><button><img src="/eliobones/dbhell-bones/apple-touch-icon.png"><br>dbhell-bones</button></a></p>
</figure>
  <figure>
  <a href="/eliobones/mongoose-bones/artwork/splash.jpg" target="_splash"><img src="/eliobones/mongoose-bones/artwork/splash.jpg" target="_splash"></a>
  <h3>MongooseJs</h3>
  <p>Restful API with endpoints to handle <a href="https://schema.org">schema.org</a> data and save it in a MongoDb, the <strong>elioWay</strong>.</p>
  <p><a href="/eliobones/mongoose-bones/"><button><img src="/eliobones/mongoose-bones/apple-touch-icon.png"><br>mongoose-bones</button></a></p>
</figure>
</section>

## Related

<article>
  <a href="/eliothing/">
  <img src="/eliothing/apple-touch-icon.png">
  <div>
  <h4>eliothing</h4>
  <p>Gets the Schema for your <strong>eliobones</strong>, the <strong>elioWay</strong>.
    </p>
</div>
</a>
</article>

<article>
  <a href="/elioflesh/">
  <img src="/elioflesh/apple-touch-icon.png">
  <div>
  <h4>elioflesh</h4>
  <p>Client apps for your <strong>eliobones</strong>, the <strong>elioWay</strong>.
    </p>
</div>
</a>
</article>
