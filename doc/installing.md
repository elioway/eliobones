# Installing eliobones

- [eliobones Prerequisites](/eliobones/prerequisites.html)

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliobones.git
cd eliobones
git submodule init
git submodule update
```
