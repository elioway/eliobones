# Permits

We'll do Permissions like this:

```
takeupT Kind_of_Blue thisismypasswordletmein --mainEntityOfPage=MusicAlbum
loginT Kind_of_Blue thisismypasswordletmein
takeupT Kind_of_Blue
# let everyone read this record,
takeonT Kind_of_Blue readTAllAnonymousUsers
  --validFor=readT
  --mainEntityOfPage=Permit
  --Permit.permitAudience=ANON
  --Permit.validUntil="2024-01-01"
# let my mate update records.
takeonT Kind_of_Blue "allowMyMateDaveUpdateTPermission"
  --validFor=updateT
  --mainEntityOfPage=Permit
  --Permit.permitAudience=myMateDave
  --Permit.validUntil="2023-01-01"
# banned for now.
takeonT Kind_of_Blue "myMateSueIsBanned"
--validFor="*"
--mainEntityOfPage=Permit
--Permit.permitAudience=myMateSue
--Permit.validFrom="2035-01-01"
```

THis is more revolutionary than it looks. Consider this and get back to me:

```
takeupT Kind_of_Blue thisismypasswordletmein --mainEntityOfPage=MusicAlbum
loginT Kind_of_Blue thisismypasswordletmein
takeupT Kind_of_Blue
# let everyone read this record,
takeonT Kind_of_Blue readTAllAnonymousUsers
  --validFor=readT
  --mainEntityOfPage=Permit
  --Permit.permitAudience=ANON
  --Permit.validUntil="2024-01-01"
# let my mate update records.
takeonT readTAllAnonymousUsers "allowMyMateDaveUpdateTPermission"
  --validFor=updateT
  --mainEntityOfPage=Permit
  --Permit.permitAudience=myMateDave
  --Permit.validUntil="2023-01-01"
```
