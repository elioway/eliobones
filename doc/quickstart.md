# Quickstart eliobones

- [eliobones Prerequisites](/eliobones/prerequisites.html)
- [Installing eliobones](/eliobones/installing.html)

## Nutshell

Restful eliobones API implement a common standard.

### Anatomy of an **eliobones** API

### Auth

- **loginT** `POST` `/auth/:T` Exchange a username and password for a Permit. _Unprotected._
- **logoutT** `DELETE` `/auth/:T/:id` Expire the Permit.

### Engage

- **takeupT** `POST` `/engage/:T` "Takeup" a Thing of _schemaType_ `:T`, e.g. "Thing", "MedicalCondition", "Vehicle". _Unprotected._
- **updateT** `PATCH` `/engage/:T/:id` Update an _engaged_ Thing.
- **readT** `GET` `/engage/:T/:id` View the _engaged_ Thing.
- **destroyT** `DELETE` `/engage/:T/:id` Delete an _engaged_ Thing (doesn't destroy any Things in its _list_).

### List

- **takeonT** `POST` `/list/:T/:id/:LT` "Takeup" then "TakeOn" a new Thing of _schemaType_ `:LT` and add it to the _engaged_ Thing's _list_.
- **listT** `GET` `/list/:T/:id` View the _engaged_ Thing's _list_.
- **enlistT** `PUT` `/list/:T/:id/:LT/:lid` En _list_ an existing Thing.
- **unlistT** `DELETE` `/list/:T/:id/:LT/:lid` Un _list_ an existing Thing.

### Schema

- **schemaT** `GET` `/schema/:T` Get the Meta Schema of _schemaType_ `:T`. _Unprotected._
- **schemaT** `GET` `/schema/:t` Get an empty instance of _schemaType_ `:t`. _Unprotected._

The idea. A bit like how nedb works, but without the merge.

- TODO: A Database starts with a FROZEN initial state.
- TODO: All the above actions get stored as changes.
- TODO: Existing "intime" changes are played by default on the app start.
- TODO: New Actions are added to the "intime" database of changes.
- TODO: Actions are BLOCKCHAIN transactions?

## Bones APIs

All **eliobones** APIs should, by design, enforce the priciple's of **elioWay**.

- [A cult, not a framework](/a-cult-not-a-framework)

All **eliobones** APIs should provide endpoints for data modelled on every <https://schema.org> documented _schemaTypes_.

Largely speaking **eliobones** APIs handle standard requests like `PATCH`, `DELETE`, `GET` as you would expect, except with a couple of notable exceptions which are detailed below.

### SchemaType is a parameter

Where `http://bones.api/` is your domain, there should always be a parameter for _schemaType_, e.g

- `GET` `http://bones.api/:engage/:id/`

We will call this the **engage** parameter, representing the _schemaType_ being engaged (duh!).

## `schema` Endpoint

There should always be an endpoint which can respond with the meta information of a given _schemaType_. The client can use this meta data to auto-generate the MVC classes, or simply to render a form with inputs tailored to the fields of the _schemaType_.

- `http://bones.api/schema/:engage/`

Takes 1 parameter:

- `engage`: The _schemaType_ you want to retrieve.

Examples:

- `http://bones.api/schema/Thing/`
- `http://bones.api/schema/Hospital/`
- `http://bones.api/schema/CreativeWork/`
- `http://bones.api/schema/MedicalImagingTechnique/`
- `http://bones.api/schema/FoodEstablishmentReservation/`

## `signup` Endpoint

In **eliobones** there is no separate Auth model. We take the view that because one `Thing` isn't so unlike another, the Auth Model will just be one of the many _schemaTypes_.

- `POST` `http://bones.api/signup/:engage/`

- Posting to `http://bones.api/signup/Thing/` will create a new `Thing`, as long as you provide `username` and `password`. Signin to **engage** it.

- Posting to `http://bones.api/auth/signup/Hospital/` will create a new `Hospital`, as long as you provide `username` and `password`. Signin to **engage** it.

- Whether you Signin to **engage** a `Thing` or `Hospital` makes no difference, and you can start to **list**, **engage** and **iterate** on either. However, if you enter the "account settings" of the signed in `Hospital`, you will be able to edit the `Thing` fields, the Extra fields for `Hospital` including `Place` fields (which `Hospital` subclasses).

- In the App of Apps, the signedin `Thing` is the App.

## `login` and `logout` Endpoints

These endpoints do not need parameters. Simply post the username and password (or credentials) to `login`, and send nothing except an active session to `logout`.

- `POST` `http://bones.api/login/`
- `GET` `http://bones.api/logout/`

## `get` Endpoint

Returns a single `Thing` with all its fields.

- `GET` `http://bones.api/:engage/:id/`

Takes 2 parameters:

- `engage`: The _schemaType_ you want to **engage**. Even though it isn't required, we suggest including this as it has semantic value.
- `id` of the Thing you want to **engage**

## `list` Endpoint

Returns a _slim_ list of Things belonging to the `list` property of the **engaged** `Thing`.

- `GET` `http://bones.api/:engage/:id/list`

Takes the same parameters as `get`

Comparing `get` with get `list`:

- `http://bones.api/Hospital/Abc123/` responds with the **engaged** `Hospital`s fields including the **list** field as an Array of MongooseObjectIDs.
- `http://bones.api/Hospital/Abc123/list/` responds with the **engaged** `Hospital`s **list** field as an Array of _slim_ JSON objects, with core fields such as `id`, `name`, `description`, `image`, etc - which you can render as `ul` or `table`.

## `list` with Filter Endpoint

Returns a _slim_ list of Things belonging to the list property of the **engaged** Thing which are filtered to just one _schemaType_.

- `GET` `http://bones.api/:engage/:id/list/:T`

Takes the same parameters as `get` +

- `T`: Filter the list to this given _schemaType_.

Examples:

- `http://bones.api/FoodEstablishment/Abc123/list/MenuItems`
- `http://bones.api/FoodEstablishment/Abc123/list/FoodEstablishmentReservation`

## `create` Endpoint

In **eliobones** you can't `POST` to CREATE a new Thing to a parameterless URL. New Things are always created for an **engaged** Thing, and added to its **list**.

_Starting from scratch, you should use the `signup` endpoint described above._

- Posting to `http://bones.api/Thing/` _WON'T_ create a new `Thing`.
- Posting to `http://bones.api/Hospital/Abc123/MedicalImagingTechnique` _WILL_ create a new `MedicalImagingTechnique` and add it to `Hospital` Abc123's **list**.

- `POST` `http://bones.api/:engage/:id/:T`

Takes the same parameters as `get` +

- `T`: The _schemaType_ of the Thing being added to the **engaged** Thing's list.

Examples:

- `http://bones.api/FoodEstablishment/Abc123/FoodEstablishmentReservation`
- `http://bones.api/FoodEstablishment/Abc123/MenuSection`
- `http://bones.api/MenuSection/Def456/MenuItem`

## `update` and `delete` Endpoints

These work as you'd expect:

- `PATCH` `http://bones.api/:engage/:id/`
- `DELETE` `http://bones.api/:engage/:id/`

Both take the same parameters as `get`.

## Creating a new eliobones app

1. Use `yo thing` to create a new app `elioway/eliobones/bones-inFramework`.
2. Build a Restful API in the framework of your choice.
3. If required: Create a new Schema Builder in `your/repo/elioway/eliothing/thing-inFramework` folder.
4. Get it working.
5. Test it.
6. Document it using [chisel](/elioangels/chisel).
7. Brand it using [generator-art](/elioangels/generator-art). Tell everyone about it.
8. Use it to do things **the elioWay**.
9. Push it to the <https://gitlab.com/eliobones/>.
