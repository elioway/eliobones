const destroyT = require("./destroyT")
const enlistT = require("./enlistT")
const listT = require("./listT")
const pingT = require("./pingT")
const readT = require("./readT")
const schemaT = require("./schemaT")
const takeonT = require("./takeonT")
const takeupT = require("./takeupT")
const unlistT = require("./unlistT")
const updateT = require("./updateT")

const inviteT = require("./inviteT")
const optimizeT = require("./optimizeT")
const undoT = require("./undoT")

module.exports = {
  destroyT,
  enlistT,
  inviteT,
  listT,
  pingT,
  readT,
  schemaT,
  takeonT,
  takeupT,
  updateT,
  unlistT,
  optimizeT,
  undoT,
}
