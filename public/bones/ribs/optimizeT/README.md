![](https://elioway.gitlab.io/ribs/optimizeT/elio-optimize-t-logo.png)

> OptimizeT, **the elioWay**

# optimizeT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [optimizeT Documentation](https://elioway.gitlab.io/ribs/optimizeT/)

## Prerequisites

- [optimizeT Prerequisites](https://elioway.gitlab.io/ribs/optimizeT/installing.html)

## Installing

- [Installing optimizeT](https://elioway.gitlab.io/ribs/optimizeT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [optimizeT Quickstart](https://elioway.gitlab.io/ribs/optimizeT/quickstart.html)

# Credits

- [optimizeT Credits](https://elioway.gitlab.io/ribs/optimizeT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/optimizeT/apple-touch-icon.png)
