![](https://elioway.gitlab.io/ribs/takeupT/elio-takeup-t-logo.png)

> TakeupT, **the elioWay**

# takeupT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [takeupT Documentation](https://elioway.gitlab.io/ribs/takeupT/)

## Prerequisites

- [takeupT Prerequisites](https://elioway.gitlab.io/ribs/takeupT/installing.html)

## Installing

- [Installing takeupT](https://elioway.gitlab.io/ribs/takeupT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [takeupT Quickstart](https://elioway.gitlab.io/ribs/takeupT/quickstart.html)

# Credits

- [takeupT Credits](https://elioway.gitlab.io/ribs/takeupT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/takeupT/apple-touch-icon.png)
