![](https://elioway.gitlab.io/spine/authT/elio-auth-t-logo.png)

> AuthT, **the elioWay**

# authT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [authT Documentation](https://elioway.gitlab.io/spine/authT/)

## Prerequisites

- [authT Prerequisites](https://elioway.gitlab.io/spine/authT/installing.html)

## Installing

- [Installing authT](https://elioway.gitlab.io/spine/authT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [spine Quickstart](https://elioway.gitlab.io/spine/quickstart.html)
- [authT Quickstart](https://elioway.gitlab.io/spine/authT/quickstart.html)

# Credits

- [authT Credits](https://elioway.gitlab.io/spine/authT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/spine/authT/apple-touch-icon.png)
