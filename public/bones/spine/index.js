const authT = require("./authT")
const engageT = require("./engageT")
const permitT = require("./permitT")

module.exports = {
  authT,
  engageT,
  permitT,
}
